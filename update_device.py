# update_device.py
#
# This Python script update the attributes of a Meraki Switch with the matching input of a xlsx file.
#
# Python3 is required and the following modules
# pandas
#
# To Start the script, enter in your CLI in the script directory
#
#  python3 update_device.py -k <Meraki API key> -o <org name> -n <network name> -f <excel file>
#
# Example:
# python3 meraki_update_device.py -k "********************************6e9212c" -o "MyOrg" -n "MyNetwork" -f "input.xlsx"
#
# based of github Meraki : https://github.com/meraki/automation-scripts
# Modification 13/08/2019
# Axel BRUN

import pandas as pd
import getopt
from lib_meraki_AB import *


# update ports with excel
def update_device_from_excel(arg_api_key, file, net_id):
    """
    read the excel file and execute the corresponding requests

    :param arg_api_key: string
    :param file: string (path to the excel file)
    :param net_id: string
    :return: "done"
    """
    # read the excel and put the different columns into categories using pandas
    input_df = pd.read_excel(file, dtype=str)
    input_list_serial = input_df['serial'].tolist()
    input_list_name = input_df['name'].tolist()
    input_list_tags = input_df['tags'].tolist()
    input_list_address = input_df['address'].tolist()

    for elem_serial in input_list_serial:
        # convert to serial list
        index_elem = input_list_serial.index(elem_serial)
        print("")
        print("")
        print("claiming ", elem_serial)
        claim_device(arg_api_key, net_id, elem_serial)
        print("")
        print("")
        print("setting ", elem_serial)
        # API request
        update_device(arg_api_key, net_id, elem_serial,
                      input_list_name[index_elem],
                      input_list_tags[index_elem],
                      input_list_address[index_elem])

    return "done"


def main(argv):
    api_run = api_meraki_run(argv)
    arg_apikey = api_run[0]
    nwidlist = api_run[2]
    arg_file = api_run[3]
    # loop through the network ID list and process all of their devices
    for i in range(0, len(nwidlist)):

        update_device_from_excel(arg_apikey, arg_file, nwidlist[i])

    print('INFO: End of script')


if __name__ == '__main__':
    main(sys.argv[1:])

