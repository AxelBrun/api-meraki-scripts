# meraki_change_ports.py
#
# This Python script update the ports of a Meraki Switch with the matching input of a xlsx file.
# 
# Python3 is required and the following modules
# pandas
#
# To Start the script, enter in your CLI in the script directory
#
#  python3 meraki_change_ports.py -k <Meraki API key> -o <org name> -n <network name> -f <excel file>
#
# Example:
# python3 meraki_change_ports.py -k "********************************6e9212c" -o "MyOrg" -n "MyNetwork" -f "input.xlsx"
#
# based of github Meraki : https://github.com/meraki/automation-scripts
# Modification 13/08/2019
# Axel BRUN

import pandas as pd
import getopt
from lib_meraki_AB import *




# update ports with excel
def update_ports_from_excel(arg_api_key, file):
    """
    read the excel file and execute the corresponding requests

    :param arg_api_key: string
    :param file: string (path to the excel file)
    :return: "done"
    """
    # read the excel and put the different columns into categories using pandas
    input_df = pd.read_excel(file, sheet_name="ports", dtype=str)

    input_list_port = input_df['port'].tolist()
    input_list_tags = input_df['tags'].tolist()
    input_list_type = input_df['type'].tolist()
    input_list_vlan = input_df['vlan'].tolist()
    input_list_voice = input_df['VoiceVlan'].tolist()
    input_list_allowedvlans = input_df['allowedVlans'].tolist()

    # # TODO ?? check for multiples vlans
    # j = 0
    # for elem in input_list_allowedvlans:
    #     i = 0
    #     for carac in elem:
    #         if carac == ".":
    #             elem = elem[:i] + "," + elem[i + 1:]
    #         i += 1
    #     input_list_allowedvlans[j] = elem
    #     j += 1

    input_list_serial = pd.read_excel(file, sheet_name="serial", dtype=str)['serial'].tolist()

    for elem_serial in input_list_serial:

        print("")
        print("")
        print("setting ports of ", elem_serial)
        # API request
        for port_number in input_list_port:
            index_port = input_list_port.index(port_number)
            update_port(arg_api_key,
                        elem_serial,
                        str(port_number),
                        input_list_tags[index_port],
                        input_list_type[index_port],
                        input_list_vlan[index_port],
                        input_list_voice[index_port],
                        input_list_allowedvlans[index_port]
                        )

    return "done"

    # do the same with mac addresses instead of serials
    #
    # input_list_mac = pd.read_excel(file, sheet_name="mac", dtype=str)['mac'].tolist()
    #
    # print(pd.read_excel(file, "mac", dtype=str))
    # for elem in input_list_mac:
    #     index_elem = input_list_mac.index(elem)
    #     elem_serial = get_info_with_input(list_devices, input_list_mac[index_elem], 'mac', 'serial')
    #
    #     print("")
    #     print("")
    #     print("setting ports of ", elem)
    #     # API request
    #     for port_number in input_list_port:
    #         index_port = input_list_port.index(port_number)
    #         update_port(API_KEY, elem_serial, str(port_number), input_list_tags[index_port],
    #                     input_list_type[index_port],
    #                     input_list_vlan[index_port], input_list_voice[index_port],
    #                     input_list_allowedvlans[index_port])
    #
    # return "done"


def main(argv):
    # loop through the network ID list and process all of their devices
    api_run = api_meraki_run(argv)
    arg_apikey = api_run[0]
    nwidlist = api_run[2]
    arg_file = api_run[3]

    for i in range(0, len(nwidlist)):
        # list_devices = get_devices(arg_apikey, nwidlist[i])
        # update_ports_from_excel(arg_apikey, arg_file, list_devices)
        update_ports_from_excel(arg_apikey, arg_file)

    print('INFO: End of script')


if __name__ == '__main__':
    main(sys.argv[1:])
