# MERAKI API REQUEST 

This scripts are dedicated to simplify and automate the management of Meraki networks with python. \
A main library and several scripts are included, that can automate different actions on the Meraki networks.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 


### Prerequisites



Python3 \
See https://www.python.org/downloads/

Modules :\
    - Pandas\
    - Requests\
    - Xlrd

```
pip3 install pandas (processing dataframe)
pip3 install requests (api requests)
pip3 install xlrd (reading xl files)
```

An API Meraki key is required for your org, see the Meraki dashboard. This key is not to be shared with anyone.

### Installing

Clone this repo. TODO

## Running the tests

TODO
## lib_meraki_AB

This is the main part of all the script, as it act as a module for all meraki actions and getting the scripts started.

## Using the scripts

Use this pattern into your CLI : 
```
# python scriptname.py -k "********************************6e9212c" -o "MyOrg" -n "MyNetwork" -f "input_file.xlsx"
```

#### Change ports configurations

Copy the "input_port_template", name it "input_ports.xlsx", fill the file with your infos. Launch this command into your CLI :
```
# python meraki_change_ports.py -k "********************************6e9212c" -o "MyOrg" -n "MyNetwork" -f "input_ports.xlsx"
```


#### Update device attributes
Copy the "input_device_template", name it "input_device.xlsx", fill the file with your infos. Launch this command into your CLI :
```
# python update_device.py -k "********************************6e9212c" -o "MyOrg" -n "MyNetwork" -f "input_device.xlsx"
```
#### Scan ports

Other function are in development.

## Authors

* **Axel BRUN** 

## Acknowledgments

* based of github Meraki : https://github.com/meraki/automation-scripts