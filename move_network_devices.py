# move_network_devices.py
#
# This Python script saves the attributes of all meraki switches from a network into separates xlsx files
# Then you can move some of the devices from one network to another,
# with most configuration saved  and re-applied in the new network
#
# Python3 is required and the following modules
# pandas
#
# To Start the script, enter in your CLI in the script directory
#
#  python3 move_network_devices.py -k <Meraki API key> -o <org name> -n <network name> - f "no"
#
# Example:
# python move_network_devices.py -k "********************************6e9212c" -o "MyOrg" -n "MyNetwork" -f
#
#

# Modification 22/11/2019
# Axel BRUN

import pandas as pd
from pandas import ExcelWriter
import os
import getopt
from lib_meraki_AB import *


def save_network_devices(api_key, net_id):
    """
    :param api_key: string , your meraki api key
    :param net_id: string , your meraki network id

    :does: creates xlsx files for every device, with one page for main attributes,
        one for uplink info, one for ports settings

    :return: "done"
    """

    # list all Meraki Switches in the network
    list_ms = get_ms(api_key, net_id)
    list_mr = get_mr(api_key, net_id)

    # for every single MR
    current = 0
    for mr in list_mr:
        current += 1

        print("Saving MR " + str(current) + "/" + str(len(list_mr) + 1))
        # path to save the file
        arg_output = "outputs/" + mr['name'] + ".xlsx"

        # delete if already existing
        if os.path.isfile(arg_output):
            os.remove(arg_output)

        # converts dict to df using dictionary keys as rows
        mr_df = pd.DataFrame.from_dict(mr, orient='index', columns=['value'])

        # get the uplink info and turn them into a df
        uplink = get_mgmt_inter(api_key, net_id, mr['serial'])['wan1']

        if uplink['usingStaticIp'] and isinstance(uplink['staticDns'], list):
            # split dns list into string
            uplink['staticDns'] = ','.join(uplink['staticDns'])

        # converts to df
        uplink_df = pd.DataFrame([uplink], index=["value"]).T

        # write the new xlsx files
        writer = ExcelWriter(arg_output)
        mr_df.to_excel(writer, sheet_name="device")
        uplink_df.to_excel(writer, sheet_name="uplink")
        writer.save()

    # for every single MS
    current = 0
    for ms in list_ms:
        current += 1
        print("Saving MR " + str(current) + "/" + str(len(list_mr) + 1))
        # path to save the file
        arg_output = "outputs/" + ms['name'] + ".xlsx"

        # delete if already existing
        if os.path.isfile(arg_output):
            os.remove(arg_output)

        # converts dict to df using dictionary keys as rows
        ms_df = pd.DataFrame.from_dict(ms, orient='index', columns=['value'])

        # get the uplink info and turn them into a df
        uplink = get_mgmt_inter(api_key, net_id, ms['serial'])['wan1']
        # split dns list into string
        if uplink['usingStaticIp']:
            uplink['staticDns'] = ','.join(uplink['staticDns'])
        # converts to df
        uplink_df = pd.DataFrame([uplink], index=["value"]).T

        # get the ports info and turn them into a df
        ports = get_ports(api_key, ms['serial'])
        ports_df = pd.DataFrame.from_dict(ports).set_index('number')

        # write the new xlsx files
        writer = ExcelWriter(arg_output)
        ms_df.to_excel(writer, sheet_name="device")
        uplink_df.to_excel(writer, sheet_name="uplink")
        ports_df.to_excel(writer, sheet_name='ports')
        writer.save()

    return


def move_network_devices(api_key, src_net_id, dst_net_id):
    """
    :param api_key: string , your meraki api key
    :param src_net_id: string , your source meraki network id
    :param dst_net_id: string , your destination meraki network id

    :does: filter by file name (starting with)
            delete the devices from source network
            add then to deestination network
            configure them accordingly to the file saved earlier

    :return: "done"
    """
    ui_filter_name = input("Move devices starting with  ")

    file_list = []
    for file in os.listdir("outputs/"):
        if file.startswith(ui_filter_name):
            file_list.append(file)

    for file in file_list:
        file = "outputs/" + file
        device_df = pd.read_excel(file, "device", index_col=0).replace('nan', '')

        # remove from source network
        remove_device(api_key, src_net_id, device_df.at['serial', 'value'])

        # claim into new network
        claim_device(api_key, dst_net_id, device_df.at['serial', 'value'])

        # set general settings
        update_device(api_key,
                      dst_net_id,
                      device_df.at['serial', 'value'],
                      device_df.at['name', 'value'],
                      device_df.at['tags', 'value'],
                      device_df.at['address', 'value']
                      )

        uplink_df = pd.read_excel(file, "uplink", index_col=0).replace('nan', '')

        # set uplink settings
        # for static config
        if uplink_df.at['usingStaticIp', 'value']:
            put_mgmt_inter(api_key,
                           dst_net_id,
                           device_df.at['serial', 'value'],
                           uplink_df.at['usingStaticIp', 'value'],
                           uplink_df.at['staticIp', 'value'],
                           uplink_df.at['staticGatewayIp', 'value'],
                           uplink_df.at['staticSubnetMask', 'value'],
                           uplink_df.at['staticDns', 'value'],
                           uplink_df.at['vlan', 'value']
                           )

        # for dbcp configs
        else:
            put_mgmt_inter(api_key,
                           dst_net_id,
                           device_df.at['serial', 'value'],
                           False,
                           uplink_df.at['vlan', 'value']
                           )

        # for Meraki Switches
        # set ports
        if device_df.at['model', 'value'].startswith('MS'):
            ports_df = pd.read_excel(file, "ports", index_col=0)
            ports_df = ports_df.fillna("no")

            port_number = 0
            for index, row in ports_df.iterrows():
                port_number += 1
                update_port(api_key,
                            dev_serial=device_df.at['serial', 'value'],
                            port_number=str(port_number),
                            tags=row['tags'],
                            enabled=row['enabled'],
                            poeEnabled=row['poeEnabled'],
                            _type=row['type'],
                            vlan=row['vlan'],
                            voiceVlan=row['voiceVlan'],
                            allowed_vlans=row['allowedVlans']
                            )

    return


def main(argv):
    ui_save = input("Save network devices ? (y/n) ")
    if ui_save == "y":

        argv[5] = input("Source Network name ? ")

        api_run = api_meraki_run(argv)
        arg_apikey = api_run[0]
        nwidlist = api_run[2]

        # loop through the network ID list

        for i in range(0, len(nwidlist)):
            net_id = nwidlist[i]
            save_network_devices(arg_apikey, net_id)

    ui_move = input("Move devices to another network ? (y/n) ")
    if ui_move == "y":

        api_run = api_meraki_run(argv)
        arg_apikey = api_run[0]
        s_nwidlist = api_run[2]

        dst_net_name = input("Destination network name ? ")

        argv[5] = dst_net_name
        api_run = api_meraki_run(argv)
        d_nwidlist = api_run[2]

        # loop through the network ID list

        for i in range(0, len(s_nwidlist)):
            net_id = s_nwidlist[i]
            dst_net_id = d_nwidlist[i]
            move_network_devices(arg_apikey, net_id, dst_net_id)


if __name__ == '__main__':
    main(sys.argv[1:])
