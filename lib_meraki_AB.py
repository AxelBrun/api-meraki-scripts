# lib_Meraki_AB
# update 22/10/2019
# meraki module for API based Python Scripts
# based of github Meraki : https://github.com/meraki/automation-scripts
# AXEL BRUN

import requests
import json
import time
import sys
import getopt
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

API_EXEC_DELAY = 0.21  # used to fit the limit of 5 API requests per seconds
baseUrl = "https://api.meraki.com/api/v0"
null = None

# ------------------------------------------
# Functions for sending API requests
# ------------------------------------------

# function calling the API request
# return the request's response
def call_api_m(api_key_maj, url, req_type, body):
    """ function calling the API request

    -------inputs--------

    api_key_maj : User's Meraki API Key
    url : API url that is to be reached
    type ("put", "post" or "get")
    body (dict type) : API request Body or {} if no body


    -------outputs--------

    request's reponse (json type) """

    print("")
    print("Calling API", url)
    time.sleep(API_EXEC_DELAY)  # fit the limit fixed by meraki on their API
    if body != {}:
        print(body)
    data_json = json.dumps(body)  # convert body to json

    # standard for API Meraki
    head = {
        'X-Cisco-Meraki-API-Key': api_key_maj,
        'Content-Type': 'application/json'
    }

    # call the api matching the input parameters
    # r is the response from API
    if req_type == "put":
        r = requests.put(
            url=url,
            headers=head,
            data=data_json,
            verify=False
        )

    elif req_type == "get":
        r = requests.get(
            url=url,
            headers=head,
            data=data_json,
            verify=False
        )
    elif req_type == "post":
        r = requests.post(
            url=url,
            headers=head,
            data=data_json,
            verify=False
        )
    else:
        print("error in req type : ", req_type)
        r = "error"

    print("________")
    if r.ok:
        print(r, "in", r.elapsed.total_seconds(), "s")
    else:
        print(r, "error")
        print(r.text)
        sys.exit("bad response from API")

# TODO fix that
    try:
        rjson = r.json()
    except json.decoder.JSONDecodeError:
        rjson = "no json"
        return rjson
    return rjson


# from GitHub Meraki, customized
# return the org_id with given name
def getorgid(p_apikey, p_orgname):
    time.sleep(API_EXEC_DELAY)
    print("Calling API get_org")
    # looks up org id for a specific org name
    # on failure returns 'null'
    url = 'https://dashboard.meraki.com/api/v0/organizations'
    print(url)
    time.sleep(API_EXEC_DELAY)
    r = requests.get(
        url,
        headers={'X-Cisco-Meraki-API-Key': p_apikey, 'Content-Type': 'application/json'},
        verify=False)
    print(r, "in", r.elapsed.total_seconds(), "s")
    print("")
    if r.status_code != requests.codes.ok:
        return 'null'

    rjson = r.json()

    for record in rjson:
        if record['name'] == p_orgname:
            return record['id']
    return 'null'


# from GitHub Meraki,customized
def getshardurl(p_apikey, p_orgid):
    # Looks up shard URL for a specific org. Use this URL instead of 'dashboard.meraki.com'
    # when making API calls with API accounts that can access multiple orgs.
    # On failure returns 'null'
    time.sleep(API_EXEC_DELAY)
    print("Calling API get_SNMP_Settings")
    url = 'https://dashboard.meraki.com/api/v0/organizations/%s/snmp' % p_orgid
    print(url)
    r = requests.get(
        url,
        headers={'X-Cisco-Meraki-API-Key': p_apikey, 'Content-Type': 'application/json'},
        verify=False)
    print(r, "in", r.elapsed.total_seconds(), "s")
    print("")
    if r.status_code != requests.codes.ok:
        return 'null'
    rjson = r.json()
    return rjson['hostname']


# from GitHub Meraki
def getnetworks(p_apikey, p_shardurl, p_orgid):
    # returns a list of all networks associated to an organisation ID.
    r = requests.get(
        'https://%s/api/v0/organizations/%s/networks' % (p_shardurl, p_orgid),
        headers={'X-Cisco-Meraki-API-Key': p_apikey, 'Content-Type': 'application/json'},
        verify=False)
    returnvalue = []
    if r.status_code != requests.codes.ok:
        returnvalue.append('null')
        return returnvalue
    rjson = r.json()
    for i in range(0, len(rjson)):
        returnvalue.append(rjson[i]['id'])
    return returnvalue


# from GitHub Meraki
def getnwid(p_apikey, p_shardurl, p_orgid, p_nwname):
    # looks up network id for a network name
    # on failure returns 'null'

    r = requests.get('https://%s/api/v0/organizations/%s/networks' % (p_shardurl, p_orgid),
                     headers={'X-Cisco-Meraki-API-Key': p_apikey, 'Content-Type': 'application/json'},
                     verify=False)

    if r.status_code != requests.codes.ok:
        return 'null'

    rjson = r.json()

    for record in rjson:
        if record['name'] == p_nwname:
            return record['id']
    return 'null'


# ------------------------------------------
# Functions clients :
# ------------------------------------------

# post request, update the attributes of a client with a new name and device_Policy
# return the response json from the API
def update_client(api_key_maj, net_id, mac_client, name_client, device_policy):
    """
        update the attributes of a client with a new name and device_Policy
        post request
        return the response json from the API
        use the call_api_m(...) function

        -------inputs--------

        api_key_maj
        net_id
        mac_client
        type ("put", "post" or "get")
        body (dict type)
        name_request

        -------outputs--------

        request's reponse (json type)

    """
    # TODO fix the tags (not working)
    url = baseUrl + "/networks/" + net_id + "/clients/provision"
    body = {
        "mac": mac_client,
        "name": name_client,
        "devicePolicy": device_policy
    }
    return call_api_m(api_key_maj, url, "post", body)


# ------------------------------------------
# Functions devices :
# ------------------------------------------

# return list of devices in network
def get_devices(api_key_maj, net_id):
    """ return the list of all devices in the network
    each device is represented by a dictionary with the device attributes

    -------inputs--------

    api_key_maj (string)
    net_id (string)

    -------outputs--------

    list of of the devices in the network (json type)

    """
    url = baseUrl + "/networks/" + net_id + "/devices"
    a = call_api_m(api_key_maj, url, "get", {})

    return call_api_m(api_key_maj, url, "get", {})


def claim_device(api_key_maj, net_id, dev_serial):
    """
    claim the device into the network
    :param api_key_maj: string
    :param net_id: string
    :param dev_serial: string
    :return: no
    """
    list_device = get_devices(api_key_maj, net_id)
    list_serial = []
    for device in list_device:
        list_serial.append(device['serial'])

    if dev_serial not in list_serial:

        url = baseUrl + "/networks/" + net_id + "/devices/claim"
        body = {
            "serial": dev_serial
        }
        call_api_m(api_key_maj, url, "post", body)


def remove_device(api_key_maj, net_id, dev_serial):
    """
        remove the device from the network
        :param api_key_maj: string
        :param net_id: string
        :param dev_serial: string
        :return: no
        """
    list_device = get_devices(api_key_maj, net_id)
    list_serial = []
    for device in list_device:
        list_serial.append(device['serial'])

    if dev_serial in list_serial:
        url = baseUrl + "/networks/" + net_id + "/devices/" + dev_serial + "/remove"
        body = {}
        call_api_m(api_key_maj, url, "post", body)

# return list of MS in network
def get_ms(api_key_maj, net_id):
    """
        return the list of all MS devices in the network
        each device is represented by a dictionary with the device attributes
        the filter operates on the 'model field' if the model starts with "MS..."
        -------inputs--------

        api_key_maj (string)
        net_id (string)

        -------outputs--------

        list of of the MS devices in the network (json type)
    """

    list_ms = []
    # filter on the "model" field of the device
    for device in get_devices(api_key_maj, net_id):
        if device['model'][0:2] == "MS":
            list_ms.append(device)
    return list_ms

# return list of MR in network
def get_mr(api_key_maj, net_id):
    """
        return the list of all MR devices in the network
        each device is represented by a dictionary with the device attributes
        the filter operates on the 'model field' if the model starts with "MR..."
        -------inputs--------

        api_key_maj (string)
        net_id (string)

        -------outputs--------

        list of of the MR devices in the network (json type)
    """

    list_mr = []
    # filter on the "model" field of the device
    for device in get_devices(api_key_maj, net_id):
        if device['model'][0:2] == "MR":
            list_mr.append(device)
    return list_mr


# update request, update the attributes of a device with new name, tag and address
def update_device(api_key_maj, net_id, dev_serial, name, tags, address):
    """
        update request, update the attributes of a device with new name, tag and address
        based on 'call_api_m'
        -------inputs--------

        api_key_maj (string)
        net_id (string)
        dev_serial (string)
        name (string)
        tags(strings), formatted like "tag1, tag2"
        address (string)

        -------outputs--------
        response of the request (json type)

    """
    url = baseUrl + "/networks/" + net_id + "/devices/" + dev_serial
    body = {
        "name": name,
        "tags": tags,
        "address": address,
        # always true, adjust the map to correspond to the address
        "moveMapMarker": "true"
    }
    return call_api_m(api_key_maj, url, "put", body)


# return the ports of a device
def get_mgmt_inter(api_key_maj, net_id, dev_serial):
    """
        Return the management interface settings for a device
        based on 'call_api_m'
        -------inputs--------

        api_key_maj (string)
        net_id (string)
        dev_serial (string)

        -------outputs--------
        response of the request (json type)

    """
    url = baseUrl + "/networks/" + net_id + "/devices/" + dev_serial + "/managementInterfaceSettings"
    return call_api_m(api_key_maj, url, "get", {})


def put_mgmt_inter(api_key_maj, net_id, dev_serial, usingStaticIp,
                      staticIp="no", staticGatewayIp="no", staticSubnetMask="no", staticDns="no", vlan="no"):
    """
        update request, update the management interface of a device with new IP, subnet, dns and VLAN
        based on 'call_api_m'
        -------inputs--------

        api_key_maj (string)
        net_id (string)
        dev_serial (string)
        usingStaticIp, boolean
        staticIp (string, "1.2.3.4")
        staticGatewayIp (string, "1.2.3.1")
        staticSubnetMask (string, "staticSubnetMask":"255.255.255.0")
        staticDns(list, ["1.2.3.2","1.2.3.3"])
        vlan (int)

        -------outputs--------
        response of the request (json type)

    """
    url = baseUrl + "/networks/" + net_id + "/devices/" + dev_serial + "/managementInterfaceSettings"
    if usingStaticIp:

        staticDns = staticDns.split(',')
        if isNaN(vlan):
            body = {
                "usingStaticIp": usingStaticIp,
                "staticIp": staticIp,
                "staticGatewayIp": staticGatewayIp,
                "staticSubnetMask": staticSubnetMask,
                "staticDns": staticDns

            }
        else :

            body = {
                "usingStaticIp": usingStaticIp,
                "staticIp": staticIp,
                "staticGatewayIp": staticGatewayIp,
                "staticSubnetMask": staticSubnetMask,
                "staticDns": staticDns,
                "vlan": vlan
            }


    else:
        body = {
            "usingStaticIp": usingStaticIp,
            "vlan": vlan
        }

    return call_api_m(api_key_maj, url, "put", body)

# Return the list (port,equipment) of ports of the Meraki Switch
# that have a lldp connected to an equipment
def get_lldp(api_key_maj, net_id, device_serial):
    """
        get request, Return the list (port,equipment) of ports of the Meraki Switch
        that have a lldp connected to an equipment
        based on 'call_api_m'
        -------inputs--------

        api_key_maj (string)
        net_id (string)
        dev_serial (string)

        -------outputs--------
        response of the request (json type), (port, equip)

    """
    url = baseUrl + "/networks/" + net_id + "/devices/" + device_serial + "/lldp_cdp?timespan=7200"
    return call_api_m(api_key_maj, url, "get", {})


# for filtering based on the type of equipment
def get_lldp_type_equip(lldp_rjson, equip_type):
    """
        Return the list (port,equipment) of ports of the Meraki Switch
        that have a lldp connected to an equipment that match the equip_type given
        based on get_lldp
        based on 'call_api_m'
        -------inputs--------

        lldp_rjson (json) from a "get_lldp" request
        equip_type (string used for matching)
        -------outputs--------
        response of the request (json type), (port, equip)

    """
    ports_interco_ms=[]
    if 'ports' in lldp_rjson:
        ports_interco_ms = []
        for port in lldp_rjson['ports']:
            if 'lldp' in lldp_rjson['ports'][port]:
                # match on the 'systemName' field from the 'get_lldp'
                if 'systemName' in lldp_rjson['ports'][port]['lldp']:
                    if lldp_rjson['ports'][port]['lldp']['systemName'][0:len(equip_type)] == equip_type:
                        print(port, "connected to", lldp_rjson['ports'][port]['lldp']['systemName'])
                        ports_interco_ms.append((port, lldp_rjson['ports'][port]['lldp']['systemName']))

    return ports_interco_ms


# return the ports of a device
def get_ports(api_key_maj, dev_serial):
    """
        get request, the ports info of a device
        based on 'call_api_m'
        -------inputs--------

        api_key_maj (string)
        net_id (string)
        dev_serial (string)

        -------outputs--------
        response of the request (json type)

    """
    url = baseUrl + "/devices/" + dev_serial + "/switchPorts/"
    return call_api_m(api_key_maj, url, "get", {})


# return the uplink info of a device
def get_uplink(api_key_maj, dev_serial, net_id):
    """
        get request, the ports info of a device
        based on 'call_api_m'
        -------inputs--------

        api_key_maj (string)
        net_id (string)
        dev_serial (string)

        -------outputs--------
        response of the request (json type)

    """
    url = baseUrl + "/networks/" + net_id + "/devices/" + dev_serial + "/uplink"
    return call_api_m(api_key_maj, url, "get", {})


# update a single port on a device
def update_port(api_key_maj, dev_serial, port_number, name="", tags="", enabled=True,
                poeEnabled=True, _type="access", vlan=1, voiceVlan="", allowed_vlans="all"):
    """
        put request, update a single port
        based on 'call_api_m'

        -------inputs--------

        api_key_maj (string)
        net_id (string)
        dev_serial (string)
        port_number (string)
        tags (string) not implemented yet TODO implement default values and all possibilities
        type (string) "access" or "trunk"
        vlan (int)
        voice (str, or no) for voice vlan
        allowed_vlans (??? type) TODO find the type

        -------outputs--------
        response of the request (json type)

    """

    url = baseUrl + "/devices/" + dev_serial + "/switchPorts/" + port_number
    if name == "no":
        name = null
    if voiceVlan == "no":
        voiceVlan = null
    else:
        voiceVlan = int(voiceVlan)
    if allowed_vlans == "no":
        allowed_vlans = null
    if tags == "no":
        tags = null

    body = {
        "name": name,
        "tags": tags,  # TODO fix tags
        "type": _type,
        "vlan": vlan,
        "voiceVlan": voiceVlan,
        "allowed_Vlans": allowed_vlans,
        "enabled": enabled
    }
    return call_api_m(api_key_maj, url, "put", body)


# get the ip connected on each port of a switch
def get_ips_with_ports(api_key_maj, dev_serial):
    """
        get request, get the ip connected on each port of a switch
        based on 'call_api_m'

        -------inputs--------

        api_key_maj (string)
        dev_serial (string)

        -------outputs--------
        dict (port_num : ip)

    """

    # time span is fixed to 100 arbitrary
    url = baseUrl + "/devices/" + dev_serial + "/clients?timespan=100"
    # list all the clients of the device
    rjson = call_api_m(api_key_maj, url, "get", {})
    # format the response
    # dict (port_num : ip)
    dict_ports_ip = {}
    port_num = 1
    # TODO not adapted to any number of ports
    while port_num <= 24:
        for record in rjson:
            if record['switchport'] == str(port_num):
                if str(port_num) not in dict_ports_ip:
                    dict_ports_ip[str(port_num)] = []
                dict_ports_ip[str(port_num)].append(record['ip'])
        port_num += 1
    return dict_ports_ip


# ------------------------------------------
# Networking :
# ------------------------------------------

# update a static route
def update_route(api_key_maj, net_id, route_id, name=None, subnet=None, gatewayIp=None, enabled=None):
    """
        Update a static route
        based on 'call_api_m'
        -------inputs--------

        api_key_maj (string)
        net_id (string)
        route_id (string)
        name (string)
        subnet (string)
        gatewayIp (string)
        enabled (bool)

        -------outputs--------
        response of the request (json type)
        sample response :

        Successful HTTP Status: 200

        {
          "id": "10",
          "networkId": "N_1234",
          "name": "VOIP",
          "gatewayIp": "10.8.0.5",
          "subnet": "192.168.10.0/24"
        }
    """
    url = baseUrl + "/networks/" + net_id + "/staticRoutes/" + route_id

    body = {
        "name": name,
        "subnet": subnet,
        "gatewayIp": gatewayIp,
        "enabled": enabled
    }

    return call_api_m(api_key_maj, url, "put", body)


# add a static route
def add_route(api_key_maj, net_id, route_id, name=None, subnet=None, gatewayIp=None):
    """
        add a static route
        based on 'call_api_m'
        -------inputs--------

        api_key_maj (string)
        net_id (string)
        route_id (string)
        name (string)
        subnet (string)
        gatewayIp (string)


        -------outputs--------
        response of the request (json type)
        sample response :

        Successful HTTP Status: 200

        {
          "id": "10",
          "networkId": "N_1234",
          "name": "VOIP",
          "gatewayIp": "10.8.0.5",
          "subnet": "192.168.10.0/24"
        }
    """
    url = baseUrl + "/networks/" + net_id + "/staticRoutes/"

    body = {
        "name": name,
        "subnet": subnet,
        "gatewayIp": gatewayIp
    }

    return call_api_m(api_key_maj, url, "post", body)


# list the static routes in a network
def list_routes(api_key_maj, net_id):
    """
        List the static routes for this network
        based on 'call_api_m'
        -------inputs--------

        api_key_maj (string)
        net_id (string)

        -------outputs--------
        response of the request (json type)
        sample response :

        Successful HTTP Status: 200
        [
            {
              "id": "10",
              "networkId": "N_1234",
              "name": "VOIP",
              "gatewayIp": "10.8.0.5",
              "subnet": "192.168.10.0/24"
            }
        ]
    """
    url = baseUrl + "/networks/" + net_id + "/staticRoutes/"
    return call_api_m(api_key_maj, url, "get")



# ------------------------------------------
# Functions ports :
# ------------------------------------------

# return the tags of a port
def get_port_tag(api_key_maj, device_serial, port_num):
    """
        get request, get the tags assigned to a single port of a specific device
        based on 'call_api_m'

        -------inputs--------

        api_key_maj (string)
        dev_serial (string)
        port_num (string)

        -------outputs--------
        json or "" # TODO not reliable return
    """
    url = baseUrl + "/devices/" + device_serial + "/switchPorts/" + port_num
    rjson = call_api_m(api_key_maj, url, "get", {})
    if rjson['tags'] is None:
        return ""
    return rjson['tags']


# add a tag to a specific port
def add_port_tag(api_key_maj, dev_serial, port_num, tag):
    """
        put request, add a tag or several tags to a single port of a specific device
        based on 'call_api_m'
        based on get_port_tag
        -------inputs--------

        api_key_maj (string)
        dev_serial (string)
        port_num (string)
        tag (string) "tag1, tag2"

        -------outputs--------
        response of the request (json type)
    """
    # get the tags
    old_tags = get_port_tag(api_key_maj, dev_serial, port_num)
    # split them to check if the tag is in the list
    old_tag_list = old_tags.split(" ")
    new_tag_list = tag.split(" ")
    # for every new tag
    body = {}
    for tag in new_tag_list:
        # if not in the old tag list
        if tag not in old_tag_list:
            # request to add the tag
            new_tags = old_tags + " " + tag
            url = baseUrl + "/devices/" + dev_serial + "/switchPorts/" + port_num
            body = {"tags": new_tags}

    return call_api_m(api_key_maj, url, "put", body)


# delete a tag from a specific port
def del_port_tag(api_key_maj, device_serial, port_num, tag):
    """
        put request, delete a tag or several tags from a single port of a specific device
        based on 'call_api_m'
        based on get_port_tag
        -------inputs--------

        api_key_maj (string)
        dev_serial (string)
        port_num (string)
        tag (string) "tag1, tag2"

        -------outputs--------
        response of the request (json type)
    """
    old_tags = get_port_tag(api_key_maj, device_serial, port_num)
    old_tag_list = old_tags.split(" ")
    new_tag_list = tag.split(" ")

    for tag in new_tag_list:
        if tag in old_tag_list:
            new_tag_list = []
            for old_tag in old_tag_list:
                if old_tag != tag:
                    new_tag_list.append(old_tag)
            new_tags = " ".join(new_tag_list)

            url = baseUrl + "/devices/" + device_serial + "/switchPorts/" + port_num
            body = {"tags": new_tags}

            call_api_m(api_key_maj, url, "put", body)
    return


# check if a tag is associated to a specific port
def has_port_tag(api_key_maj, device_serial, port_num, tag):
    """
        put request, check if a tag is associated to a specific port
        based on 'call_api_m'
        based on get_port_tag
        -------inputs--------

        api_key_maj (string)
        dev_serial (string)
        port_num (string)
        tag (string) "tag1, tag2"

        -------outputs--------
        Boolean (true if tag is in the port)
    """
    old_tags = get_port_tag(api_key_maj, device_serial, port_num)
    old_tag_list = old_tags.split(" ")
    new_tag_list = tag.split(" ")
    for tag in new_tag_list:
        if tag not in old_tag_list:
            print(str(new_tag_list))
            print(device_serial + "does not have " + tag + " ( has " + str(old_tag_list))
            return False  # TODO correct that
    return True


# replace a tag with another on an equipment
def replace_port_tag(api_key_maj, device_serial, port_num, old_tag, new_tag):
    """
        replace a tag with another on an equipment
        # TODO replace-port-tag
    """
    if has_port_tag(api_key_maj, device_serial, port_num, new_tag):
        del_port_tag(api_key_maj, device_serial, port_num, old_tag)
        add_port_tag(api_key_maj, device_serial, port_num, new_tag)


# ------------------------------------------
# Miscellaneous functions
# ------------------------------------------

# launch the script and fetch the org id, network id, filename
# with info given by user
def api_meraki_run(argv):
    """
    args :
       Read the input in the CLI :
            -k : API Key
            -o : OrgName
            -n : NetworkName
            -f : ExcelFile

    return :
        arg_apikey,
        orgid
        nwidlist
        arg_file

    """

    # get command line arguments
    arg_apikey = 'null'
    arg_orgname = 'null'
    arg_nwname = 'null'
    arg_file = 'null'

    try:
        opts, args = getopt.getopt(argv, 'k:o:n:f:')
    except getopt.GetoptError:
        print("args error")
        sys.exit(2)

    for opt, arg in opts:

        if opt == '-k':
            arg_apikey = arg
        elif opt == '-o':
            arg_orgname = arg
        elif opt == '-n':
            arg_nwname = arg
        elif opt == '-f':
            arg_file = arg

    if arg_apikey == 'null':
        arg_apikey = input("apikey")
    if arg_orgname == 'null':
        arg_orgname = input("orgname")
    if arg_nwname == 'null':
        arg_nwname = input("nwname")
    if arg_file == 'null':
        arg_file = input("file")

    # get organization id corresponding to org name provided by user
    orgid = getorgid(arg_apikey, arg_orgname)
    if orgid == 'null':
        print('ERROR: Fetching organization failed')
        sys.exit(2)

    # get shard URL where Org is stored
    shardurl = getshardurl(arg_apikey, orgid)
    if shardurl == 'null':
        print('ERROR: Fetching Meraki cloud shard URL failed')
        sys.exit(2)

    # create a list of network ids to be processed. If a name was given, it will only have one entry
    nwidlist = []

    if arg_nwname == '/all':
        nwidlist = getnetworks(arg_apikey, shardurl, orgid)

        if nwidlist[0] == 'null':
            print('ERROR: Fetching network list failed')
            sys.exit(2)

    else:
        # get network id corresponding to network name provided by user
        nwid = getnwid(arg_apikey, shardurl, orgid, arg_nwname)
        print(arg_apikey)
        print(orgid)
        print(arg_apikey)
        print(shardurl)
        if nwid == 'null':
            print('ERROR: Fetching network failed')
            sys.exit(2)

        nwidlist.append(nwid)

    return arg_apikey, orgid, nwidlist, arg_file


# return the requested information from the device in device_list, identified with the given input and input categ)
def get_info_with_input(device_list, _input, input_categ, info):
    """
    :param device_list: from "get_devices" function
    :param _input: string corresponding with input_categ
    :param input_categ: a field from get device
    :param info: the field expected in return
    :return: string
    based on list_by_key
    """
    # index de l'élément input
    device_index = list_by_key(device_list, input_categ).index(_input)
    device_info = list_by_key(device_list, info)[device_index]
    return device_info


# return a list of items associated to the key of a json response type from 'get_device'
def list_by_key(_list, key):
    """
    return a list of items associated to the key of a json response type from 'get_device'
    :param _list: from 'get_device'
    :param key: the field you want the list of
    :return: list
    """
    list_key = []
    for elm in _list:
        list_key.append(elm[key])
    return list_key


# return the last ip digit a
def get_end_ip(ip):
    """

    :param ip: string "WWW.X.YY.Z"
    :return: string "Z"
    """

    last_3 = ip[len(ip) - 3:]
    i = 0
    for carac in last_3:
        if carac == ".":
            last_3 = last_3[i + 1:]
        i += 1
    return last_3


def isNaN(num):
    return num != num
