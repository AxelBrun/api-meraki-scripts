# Meraki scan ports

# scan ports of switches in a Meraki network,
# suggests tags to add by looking at the ip and type of equipment (lldp) connected
# following user defined criteria (excel file)
# Modification 13/09/2019
# Axel BRUN
# WIP

import os
import pandas as pd
from pandas import ExcelWriter

import sys, getopt

from lib_meraki_AB import *


# scan every port of the equipment
# output a file suggesting modifications, based of IP
def scan_port_tag_with_ip(arg_api_key, device, file, output):
    # read the criteria file
    input_df = pd.read_excel(file, sheet_name="ip", dtype=str)
    list_criteria_ip = input_df['critere_ip'].tolist()
    list_tag = input_df['tag'].tolist()
    # get the ip corresponding to ports
    dic_ip = get_ips_with_ports(arg_api_key, device['serial'])
    # creates lists for future changes
    dic_changes = {
        'name': [],
        'serial': [],
        'port': [],
        'connected_to': [],
        'add': [],
        'delete':[]
    }

    # for every port
    for port_num in dic_ip:
        # for every ip connected to this port


        for ip in dic_ip[port_num]:
            # TO FIX ?
            if type(ip) == type(""):
                end_ip = get_end_ip(ip)
                # for every criteria ip in the list
                for crit in list_criteria_ip:
                    # if the ip match the criteria
                    index_elem = list_criteria_ip.index(crit)
                    if end_ip == crit:

                        # if the tag is not already present
                        if not has_port_tag(arg_api_key, device['serial'], port_num, list_tag[index_elem]):
                            # append to the list of add needed
                            dic_changes['name'].append(device['name'])
                            dic_changes['serial'].append(device['serial'])
                            dic_changes['port'].append(port_num)
                            dic_changes['connected_to'].append(ip)
                            dic_changes['add'].append(list_tag[index_elem])
                            dic_changes['delete'].append('no')



            else:
                os.system("pause")
    return pd.DataFrame(dic_changes)


# scan every port of the equipment
# output a file suggesting modifications, based of lldp connections
# TODO vérification si a déja le tag
def scan_port_tag_lldp(arg_api_key, network, device, file, output):
    # read the criteria file
    input_df = pd.read_excel(file, sheet_name="lldp", dtype=str)
    list_equipment = input_df['equipement'].tolist()
    list_tag = input_df['tag'].tolist()

    # creates lists for future changes
    dic_changes = {
        'name': [],
        'serial': [],
        'port': [],
        'connected_to': [],
        'add': [],
        'delete' : []
    }

    # get the lldp connections
    lldp_rjson = get_lldp(arg_api_key, network, device['serial'])
    list_port_equip = []

    # for each "equipment" entry in the excel file
    for equipment in list_equipment:
        # if there are lldp connections
        if lldp_rjson:
            # list of(port,connections_to)
            list_port_equip = get_lldp_type_equip(lldp_rjson, equipment)
            # if there are  connections for this equipment
            if list_port_equip:
                index_equip = list_equipment.index(equipment)
                # add a line to the df
                for port, equip in list_port_equip:
                    if not has_port_tag(arg_api_key, device['serial'], port, list_tag[index_equip]):

                        dic_changes['name'].append(device['name'])
                        dic_changes['serial'].append(device['serial'])
                        dic_changes['port'].append(port)
                        dic_changes['connected_to'].append(equip)
                        dic_changes['add'].append(list_tag[index_equip])
                        dic_changes['delete'].append('no')
            else :
                index_equip = list_equipment.index(equipment)
                # add a line to the df
                for port, equip in list_port_equip:
                    if has_port_tag(arg_api_key, device['serial'], port, list_tag[index_equip]):

                        dic_changes['name'].append(device['name'])
                        dic_changes['serial'].append(device['serial'])
                        dic_changes['port'].append(port)
                        dic_changes['connected_to'].append(equip)
                        dic_changes['delete'].append(list_tag[index_equip])
                        dic_changes['add'].append('no')
    # convert dic to df
    return pd.DataFrame(dic_changes)


def main(argv):
    # loop through the network ID list and process all of their devices
    api_run = api_meraki_run(argv)
    arg_api_key = api_run[0]
    nwidlist = api_run[2]
    arg_input = api_run[3]
    arg_output = "output_scan.xlsx"

    for i in range(0, len(nwidlist)):

        do_scan = input("Scan For changes ? (y/n) ")
        if do_scan == "y":

            # delete if already existing
            if os.path.isfile(arg_output):
                empty_df = pd.DataFrame()
                writer = ExcelWriter(arg_output)
                empty_df.to_excel(writer, sheet_name="lldp", index=False)
                empty_df.to_excel(writer, sheet_name="ip", index=False)
                writer.save()

            list_ms = get_ms(arg_api_key, nwidlist[i])

            for device in list_ms:
                device_serial = device['serial']


                # lldp
                df_lldp = scan_port_tag_lldp(arg_api_key, nwidlist[i], device, arg_input, arg_output)

                # ip
                df_ip = scan_port_tag_with_ip(arg_api_key, device, arg_input, arg_output)
                # concat if already existing
                if os.path.isfile(arg_output):
                    df_excel = pd.read_excel(arg_output, sheet_name="lldp")
                    df_lldp = pd.concat([df_excel, df_lldp], ignore_index=True)
                    df_excel = pd.read_excel(arg_output, sheet_name="ip")
                    df_ip = pd.concat([df_excel, df_ip], ignore_index=True)

                # to_excel
                writer = ExcelWriter(arg_output)
                df_lldp.to_excel(writer, sheet_name="lldp", index=False)
                df_ip.to_excel(writer, sheet_name="ip", index=False)
                writer.save()

        print("Please check the output file")
        print("You can make changes to output file, save it and close it.")
        do_changes = input("Apply changes ? (y/n) ")

        if do_changes == "y":
            df_output = pd.read_excel(arg_output, "ip")

            for index, row in df_output.iterrows():
                add_port_tag(arg_api_key, row['serial'], str(row['port']), row['add'])

            df_output = pd.read_excel(arg_output, "lldp")

            for index, row in df_output.iterrows():
                add_port_tag(arg_api_key, row['serial'], str(row['port']), row['add'])

    print('-------------------')
    print('INFO: End of script')


if __name__ == '__main__':
    main(sys.argv[1:])
